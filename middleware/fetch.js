export default async function ({ store }) {
    if (!store.state.departments.length)
        store.dispatch('departments/fetch')
}