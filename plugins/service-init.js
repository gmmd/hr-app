import { Service } from '@/services/service'

export default (ctx) => {
    Service.prototype.$store = ctx.store
    Service.prototype.$router = ctx.app.router
}
