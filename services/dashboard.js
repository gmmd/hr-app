import { Service } from './service'
import moment from 'moment'

class Dashboard extends Service {
    constructor() {
        super();
    }

    users = process.browser ? JSON.parse(localStorage.getItem('users')) : []

    getStats() {
        let quantity = this.users.length
        let men = this.users.filter(el => el.gender == 'male').length
        let women = this.users.filter(el => el.gender == 'female').length

        let avgAge = this.users.reduce((acc, el) => {
            let now = moment()
            let birth_day = moment(el.birth_day, 'YYYY')
            return acc += now.diff(birth_day, 'years')
        }, 0)


        let nearestBDays = []
        this.users.forEach(user => {
            let now = moment(new Date())
            let bday = moment(new Date(user.birth_day), 'DD.MM')
            if ((now.diff(bday, 'days') % (bday.year() % 4 ? 365 : 366)) < 7) {
                nearestBDays.push(user)
            }
        });

        let filteredByDepartment = {}
        this.users.forEach(el => { filteredByDepartment[el.department] = [] })
        this.users.forEach(el => { filteredByDepartment[el.department].push(el) })
        Object.keys(filteredByDepartment).forEach(key => { filteredByDepartment[key] = filteredByDepartment[key].length })

        return {
            quantity,
            men,
            women,
            filteredByDepartment,
            avgAge: Math.floor(avgAge / quantity),
            nearestBDays
        }
    }
}

export default new Dashboard()
