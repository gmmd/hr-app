import { Service } from './service'

class Staff extends Service {
    constructor() {
        super();
    }

    users = process.browser ? JSON.parse(localStorage.getItem('users')) : []

    create(data) {
        Math.max(...this.users.map(({ id }) => id)) + 1
        data = { ...data, id: Math.max(...this.users.map(({ id }) => id)) + 1 }
        this.users.push(data)

        localStorage.setItem('users', JSON.stringify(this.users))
    }

    update(data) {
        this.users = this.users.map(user => user.id == data.id ? data : user)

        localStorage.setItem('users', JSON.stringify(this.users))
    }

    delete(data) {
        this.users = this.users.filter(user => user.id != data.id)

        localStorage.setItem('users', JSON.stringify(this.users))
    }

    get(id) {
        return this.users.find(user => user.id == id)
    }

    getAll({ department = '', gender = '' }) {
        if (department) {
            return this.users.filter(user => user.department == department)
        } else if (gender)
            return this.users.filter(user => user.gender == gender)
        else if (department && department)
            return this.users.filter(user => user.department == department && user.gender == gender)
        else
            return this.users
    }
}

export default new Staff()