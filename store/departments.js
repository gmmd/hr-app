import allDepartments from '@/utils/departments'

export const state = () => ({
    all: []
})

export const getters = {}

export const mutations = {
    set(state, payload) {
        state.all = payload
    }
}

export const actions = {
    fetch({ commit }) {
        commit('set', allDepartments)
    }
}
