import users from '@/utils/users'

export const getters = {
    users: () => {
        if (process.browser) {
            return JSON.parse(localStorage.getItem('users'))
        }
    }
}

export const actions = {
    nuxtClientInit() {
        if (!JSON.parse(localStorage.getItem('users'))) {
            localStorage.setItem('users', JSON.stringify(users))
        }
    }
}
